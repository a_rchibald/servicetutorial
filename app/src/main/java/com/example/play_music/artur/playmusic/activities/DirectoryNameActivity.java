package com.example.play_music.artur.playmusic.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.play_music.artur.playmusic.R;
import com.example.play_music.artur.playmusic.constants.Constants;

/**
 * Created by Artur on 16.04.2015.
 */
public class DirectoryNameActivity extends Activity {

    private TextView textView;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.directory_item_name);

        textView = (TextView) findViewById(R.id.directory_name_txt);
        textView.setText(getIntent().getStringExtra(Constants.DIRECTORY_NAME));

        imageView = (ImageView) findViewById(R.id.back);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication().getApplicationContext(), MusicActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
