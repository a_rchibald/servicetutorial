package com.example.play_music.artur.playmusic.appservice;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.example.play_music.artur.playmusic.handlers.MusicHandler;

/**
 * Created by Artur on 15.04.2015.
 */
public class AppService extends Service {

    private static MusicHandler mHandler;

    @Override
    public void onCreate() {
        super.onCreate();
        mHandler = new MusicHandler(this);
    }

    public static void playMusic(){
        mHandler.playMusic();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
