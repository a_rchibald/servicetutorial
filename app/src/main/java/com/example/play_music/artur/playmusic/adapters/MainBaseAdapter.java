package com.example.play_music.artur.playmusic.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.play_music.artur.playmusic.R;
import com.example.play_music.artur.playmusic.externalmodel.ExternalStorageModel;
import com.example.play_music.artur.playmusic.handlers.StorageHandler;

import java.util.ArrayList;

/**
 * Created by Artur on 16.04.2015.
 */
public class MainBaseAdapter extends BaseAdapter {

    private ArrayList<ExternalStorageModel> arrayList;
    private LayoutInflater inflater;
    private Context context;

    public MainBaseAdapter(LayoutInflater inflater, Context context){
        arrayList = new StorageHandler().getDirsName();
        this.inflater = inflater;
        this.context = context;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public ExternalStorageModel getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;

        if(view == null){
            view = inflater.inflate(R.layout.list_view_item, parent, false);
            ViewHolder holder = new ViewHolder();
            holder.txtTitle = (TextView) view.findViewById(R.id.list_item_id);
            holder.txtDirsNum = (TextView) view.findViewById(R.id.dirsNum);
            holder.txtFilesNum = (TextView) view.findViewById(R.id.filesNum);
            view.setTag(holder);
        }

        ViewHolder viewHolder = (ViewHolder) view.getTag();
        viewHolder.txtTitle.setText(getItem(position).getDirsName());
        viewHolder.txtDirsNum.setText(getItem(position).getDirsNum());
        viewHolder.txtFilesNum.setText(getItem(position).getFilesNum());

        return view;
    }

    private static class ViewHolder {
        TextView txtTitle;
        TextView txtDirsNum;
        TextView txtFilesNum;
    }
}
