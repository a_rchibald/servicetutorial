package com.example.play_music.artur.playmusic.handlers;

import android.content.Context;
import android.media.MediaPlayer;

import com.example.play_music.artur.playmusic.R;

/**
 * Created by Artur on 13.04.2015.
 */
public class MusicHandler {

    private MediaPlayer mPlayer;
    private Context context;

    public MusicHandler(Context context){
        this.context = context;
        mPlayer = MediaPlayer.create(context, R.raw.daniel_powter);
    }

    public void playMusic(){
        if(mPlayer == null) return;
        if(mPlayer.isPlaying())
            mPlayer.pause();
        else
            mPlayer.start();
    }
}
