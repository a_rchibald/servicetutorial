package com.example.play_music.artur.playmusic.handlers;

import android.os.Environment;

import com.example.play_music.artur.playmusic.externalmodel.ExternalStorageModel;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Artur on 13.04.2015.
 */
public class StorageHandler {

    public ArrayList<ExternalStorageModel> getDirsName(){
        ArrayList<ExternalStorageModel> stringList = new ArrayList<>();
        File[] listFiles = Environment.getExternalStorageDirectory().listFiles();
        for(File fileName : listFiles){
            stringList.add(getExternalModel(fileName));
        }
        return stringList;
    }

    public ExternalStorageModel getExternalModel(File file){
        int dirNum = 0;
        int filesNum = 0;
        File[] directories = file.listFiles();
        if(directories != null) {
            for(File listFile : directories) {
                if (listFile.isDirectory())
                    dirNum++;
                else
                    filesNum++;
            }
        }
        return new ExternalStorageModel(file.getName(), String.valueOf(dirNum), String.valueOf(filesNum));
    }

}
