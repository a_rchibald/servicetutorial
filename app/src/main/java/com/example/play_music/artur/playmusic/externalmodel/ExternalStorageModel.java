package com.example.play_music.artur.playmusic.externalmodel;

import java.io.Serializable;

/**
 * Created by Artur on 16.04.2015.
 */
public class ExternalStorageModel implements Serializable{

    private static final long serialVersionUID = -4440796483799503968L;

    private String dirsName;
    private String dirsNum;
    private String filesNum;

    public ExternalStorageModel(String dirsName, String dirsNum, String filesNum) {
        this.dirsName = dirsName;
        this.dirsNum = dirsNum;
        this.filesNum = filesNum;
    }

    public String getDirsName() {
        return dirsName;
    }

    public void setDirsName(String dirsName) {
        this.dirsName = dirsName;
    }

    public String getDirsNum() {
        return dirsNum;
    }

    public void setDirsNum(String dirsNum) {
        this.dirsNum = dirsNum;
    }

    public String getFilesNum() {
        return filesNum;
    }

    public void setFilesNum(String filesNum) {
        this.filesNum = filesNum;
    }
}
