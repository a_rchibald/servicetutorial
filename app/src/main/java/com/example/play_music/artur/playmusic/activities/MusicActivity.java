package com.example.play_music.artur.playmusic.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.example.play_music.artur.playmusic.R;
import com.example.play_music.artur.playmusic.adapters.MainBaseAdapter;
import com.example.play_music.artur.playmusic.appservice.AppService;
import com.example.play_music.artur.playmusic.constants.Constants;
import com.example.play_music.artur.playmusic.handlers.MusicHandler;
import com.example.play_music.artur.playmusic.handlers.StorageHandler;


public class MusicActivity extends Activity implements View.OnClickListener {

    private ImageButton playBtn, pauseBtn;
    private MusicHandler mHandler;
    private StorageHandler storageHandler = new StorageHandler();
    private TextView supportMusicText;
    private Animation animation;
    private IntentFilter iFilter = new IntentFilter(getPackageName());
    private BroadcastReceiver bReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);

        mHandler = new MusicHandler(this);
        storageHandler.getDirsName();

        initWidgets();
        animation = AnimationUtils.loadAnimation(this, R.anim.music_status_animation);

        final MainBaseAdapter adapter = new MainBaseAdapter(getLayoutInflater(), this);
        ListView listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String dirName = adapter.getItem(position).getDirsName();
                Intent intent = new Intent(getApplication().getApplicationContext(), DirectoryNameActivity.class);
                intent.putExtra(Constants.DIRECTORY_NAME, dirName);
                startActivity(intent);
            }
        });

        startService(new Intent(this, AppService.class));
    }

    private void initWidgets(){
        supportMusicText = (TextView) findViewById(R.id.music_txt_annotation);
        findViewById(R.id.play_btn).setOnClickListener(this);
        findViewById(R.id.pause_btn).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.play_btn : AppService.playMusic();
                                 supportMusicText.setText(R.string.support_play_txt);
                                 supportMusicText.startAnimation(animation);
                break;
            case R.id.pause_btn : AppService.playMusic();
                                  supportMusicText.setText(R.string.support_pause_txt);
                                  supportMusicText.startAnimation(animation);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, AppService.class));
    }
}
